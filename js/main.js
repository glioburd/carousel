class Carousel {

    /**
     * 
     * @param {HTMLElement} element 
     * @param {Object} options 
     * @param {Object} [options.slidesToScroll=1] Nombre d'éléménts à faire défiler
     * @param {Object} [options.slidesVisible=1] Nombre d'élements  visibles dans un slide
     * @param {Boolean} [options.loop=false] Si on veut loop à l'infini ou pas
     */

    constructor(element, options = {}){
        this.element = element;
        // On utilise Object assigne car s'il manque une option, ça peut poser problème.
        // On assigne à un objet vide ces deux valeurs par défaut. Puis on prend les options données en argument.
        // S'il manque un paramètre dans les options, on prendra par défaut ceux parametrés si dessous.
        this.options = Object.assign({}, {
            slidesToScroll: 1,
            slidesVisible: 1,
            loop: false
        }, options);
        let children = [].slice.call(element.children);
        this.isMobile = false;
        this.currentItem = 0; // C'est actuellement l'élément visible, sera augmenté ou diminué avec prev et next
        this.moveCallbacks = [];

        // Modification du DOM
        this.container = this.createDivWithClass('carousel__container');
        this.items = children.map((child) => {
            let item = this.createDivWithClass('carousel__item');
            item.appendChild(child);
            this.container.appendChild(item);
            return item;
        });
        this.setStyle();
        this.root = this.createDivWithClass('carousel');
        this .root.setAttribute('tabindex', '0');
        this.root.appendChild(this.container);
        this.createNavigation();
        this.element.appendChild(this.root);

        // Evenements
        this.moveCallbacks.forEach(cb => cb(0)); // Par défaut, à index 0, l'élément prev sera masqué.
        this.onWindowResize();
        window.addEventListener('resize', this.onWindowResize.bind(this));
        this.root.addEventListener('keyup', e => {
            if(e.key === 'ArrowRight' || e.key === 'Right') {
                this.next();
            } else if (e.key === 'ArrowLeft' || e.key === 'Left') {
                this.prev();
            }
        });
    }

    /**
     * 
     * @param {string} className 
     * @returns {HTMLElement}
     */
    createDivWithClass (className){
        let div = document.createElement('div');
        div.setAttribute('class', className);
        return div;
    };

    createNavigation(){
        let nextButton = this.createDivWithClass('carousel__next');
        let prevButton = this.createDivWithClass('carousel__prev');
        this.root.appendChild(nextButton);
        this.root.appendChild(prevButton);
        // On bind this pour que this fasse référence à notre classe et pas à l'élément sur lequel on vient de cliquer.
        nextButton.addEventListener('click', this.next.bind(this));
        prevButton.addEventListener('click', this.prev.bind(this));

        // Si on loop pas. Si on loop, cette partie là ne sert à rien. Autrement dit : on stop maintenant si on loop.
        if (this.options.loop === true){
            return;
        }

        // On cache les boutons prev ou next si on est au début ou à la fin du carousel.
        this.onMove(index => {
            if (index === 0){
                prevButton.classList.add('carousel__prev--hidden');
            } else {
                prevButton.classList.remove('carousel__prev--hidden');
            }
            if (this.items[this.currentItem + this.slidesVisible] === undefined) {
                nextButton.classList.add('carousel__next--hidden');
            } else {
                nextButton.classList.remove('carousel__next--hidden');
            }
        })
    };

    onWindowResize() {
        let mobile = window.innerWidth < 800;
        if (mobile !== this.isMobile){
            this.isMobile = mobile;
            this.setStyle();
            this.moveCallbacks.forEach(cb => cb(this.currentItem));
        }
    }

    next(){
        this.gotoItem(this.currentItem + this.slidesToScroll);
    }

    prev(){
        this.gotoItem(this.currentItem - this.slidesToScroll);
    }

    /**
     * Déplace le carousel vers l'élément ciblé
     * @param {number} index 
     */
    gotoItem(index){
        if (index < 0){
            if (this.options.loop){
                index = this.items.length - this.slidesVisible;
            } else {
                return;
            }
        } else if (index >= this.items.length || (this.items[this.currentItem + this.slidesVisible] === undefined) && index > this.currentItem){
            if (this.options.loop) {
                index = 0;
            } else {
                return ;
            }
        }
        let translateX = index * -100 / this.items.length;
        this.container.style.transform = 'translate3d(' + translateX + '%, 0, 0)';
        this.currentItem = index;
        this.moveCallbacks.forEach(cb => cb(index));
    }
    /**
     * 
     * @param {Carousel-moveCallback} cb 
     */
    onMove(cb){
        this.moveCallbacks.push(cb);
    }

    /**
     * Applique les bonnes dimensions aux éléments du carousel
     */
    setStyle(){
        let ratio = this.items.length / this.slidesVisible;
        this.container.style.width = (ratio * 100) + "%";
        this.items.forEach(item => item.style.width = ((100 / this.slidesVisible) / ratio) + "%");
    }

    /**
     * @returns {number}
     */
    get slidesToScroll(){
        return this.isMobile ? 1 : this.options.slidesToScroll;
    }

    /**
     * @returns {number}
     */
    get slidesVisible(){
        return this.isMobile ? 1 : this.options.slidesVisible;
    }
}

document.addEventListener('DOMContentLoaded', function () {
    new Carousel(document.querySelector('#carousel1'), {
        slidesToScroll: 3,
        slidesVisible: 3,
        loop: false
    });
});

document.addEventListener('DOMContentLoaded', function () {
    new Carousel(document.querySelector('#carousel2'), {
        loop: true
    });
});